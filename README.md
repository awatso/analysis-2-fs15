#### Assistants meeting
Wednesday 2pm, break room in Y27 floor K.

#### Progress in lecture classes
* Wed 25 Feb: up to definition of compactness.
* Wed 11 Mar: up to Schwarz lemma (Bsp 3.2.2)

#### Assistant list
- Gabriel Berzunza Ojeda gabriel.berzunza@math.uzh.ch
- Cyril Marzouk cyril.marzouk@math.uzh.ch
- Quan Shi quan.shi@math.uzh.ch
- Alexander Watson * alexander.watson@math.uzh.ch
- Sophia Borowka * sborowka@physik.uzh.ch
- Ruxandra Bondarescu ruxandra@physik.uzh.ch

#### Class assignments
* Mo 08.00 - Gabriel
* Mo 15.00 - Quan
* Tu 13.00 - Alex
* We 08.00 - Cyril
* Th 08.00 - Ruxandra
* Fr 10.15 - Sophia 

Sophia and Ruxandra will have trays on floor K for students to return work.

Uncollected student work goes in a tray on floor K (abzuholende Übungsblätter)

#### Exercise sheet assignments
"Circulate" means the sheet is sent to assistants by email. "Upload" means the final sheet is uploaded on the lecture site. "Return" means students return their work. "Solutions" means solutions are uploaded on the lecture site.

* Sheet 1. **Alex** Material up to Mon 16 Feb. Circulate Fri 13 Feb. Upload 19 Feb. Return 26 Feb. Solutions 5 Mar.
* Sheet 2. **Cyril** Material up to Mon 23 Feb. Circulate Fri 20 Feb. Upload Thu 26 Feb. Return Thu 5 Mar. Solutions Fri 13 Mar.
* Sheet 3. **Gabriel** Material up to Mon 2 Mar. Circulate Fri 27 Feb. Upload Thu 5 Mar. Return Thu 12 Mar. Solutions Fri 20 Mar.
* Sheet 4. **Quan** Material up to Mon 9 Mar. Circulate Fri 6 Mar. Upload Thu 12 Mar. Return Thu 19 Mar. Solutions Fri 27 Mar.
* Sheet 5. **Sophia** Material up to Mon 16 Mar. Circulate Fri 13 Mar. Upload Thu 19 Mar. Return Thu 26 Mar. Solutions Fri 3 Apr.
* Sheet 6. **Ruxandra** Material up to Mon 23 Mar. Circulate Fri 20 Mar. Upload Thu 26 Mar. Return Thu 2 Apr. Solutions Fri 10 Apr.
* Sheet 7. **Cyril!** Material up to Mon 30 Mar. Circulate Fri 27 Mar. Upload Thu 2 Apr. Return Thu 16 Apr. Solutions Fri 24 Apr.
* Sheet 8. **Alex!** Material up to Mon 13 Apr. Circulate Fri 10 Apr (during Easter week). Upload Thu 16 Apr. Return Thu 23 Apr. Solutions Fri 1 Jun.
* Sheet 9. **Gabriel** Material up to 20 Apr. Circulate Fri 17 Apr. Upload Thu 23 Apr. Return Thu 30 Apr. Solutions Fri 8 May.
* Sheet 10. **Quan** Material up to Mon 27 Apr. Circulate Fri 24 Apr. Upload Thu 30 Apr. Return Thu 7 May. Solutions Fri 15 May.
* Sheet 11. **Sophia** Material up to Mon 4 May. Circulate Fri 1 May[^1]. Upload Thu 7 May. Return Thu 14 May. Solutions Fri 22 May.
* Sheet 12. **Ruxandra** Material up to Mon 11 May. Circulate Fri 8 May. Upload Thu 14 May. Return Thu 21 May. Solutions Fri 29 May.
* Sheet 13. **Alex** Material up to Mon 18 May. Circulate Fri 15 May. Upload Thu 21 May. Return Thu 28 May. Solutions end of semester.
* Sheet 14. **Cyril** Material up to 27 May (last lecture). Circulate Fri 22 May. Upload end of semester. Probably no return, no solutions; we will discuss this with Jean.

[^1]: Fri 3 Apr and Fri 1 May are public holidays. Let's discuss this closer to the time, but of course nobody will be expected to work on these days.

Easter is 2 April (from 16.00) to 11 April. Semester ends on Friday 30 May.
